package org.soa.rest.spring.persist;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.soa.rest.spring.model.Ticket;
import org.springframework.stereotype.Repository;

@Repository(value="ticketDAO")
public class TicketDAO extends DaoGenericoImp<Ticket, Long> implements ITicketDao{

	public Ticket getByCode(String name) {
		Map<String, Object> newMap = new HashMap<String, Object>();
		newMap.put("code", name);
		List<Ticket> listaTickets = getByProperties(newMap);
		if(listaTickets.size()>0)
			return listaTickets.get(0);
		else{
			return null;
		}
			
	}

}