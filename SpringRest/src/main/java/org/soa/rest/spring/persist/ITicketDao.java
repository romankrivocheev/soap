package org.soa.rest.spring.persist;

import org.soa.rest.spring.model.Ticket;

public interface ITicketDao extends DaoGenerico<Ticket,Long>{
	public Ticket getByCode(String name);

}
