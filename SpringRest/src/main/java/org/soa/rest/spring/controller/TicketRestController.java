package org.soa.rest.spring.controller;

import java.util.List;

import org.soa.rest.spring.model.Ticket;
import org.soa.rest.spring.persist.ITicketDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.apache.log4j.Logger;

@Controller
public class TicketRestController {
	
	private Logger log = Logger.getLogger(this.getClass());

	
	@Autowired
	private ITicketDao ticketDao;
	
	@GetMapping("/tickets")
	public List getTickets() {
		return ticketDao.getAll();
	}
	
	@GetMapping("/tickets/{code}")
	public Ticket obtenerTicket(@PathVariable("code") String code){
		Ticket ticket = ticketDao.getByCode(code);
		if(ticket == null){
			System.out.println("No se encontro en la lista");
			return null;
		}
		else
			return ticket;
	}
	
}
